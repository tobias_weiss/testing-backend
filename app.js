const express = require('express');
const path = require('path');
const session = require('express-session');
const cors = require('cors');
const mongoose = require('mongoose');
const errorHandler = require('errorhandler');
const bodyParser = require('body-parser');

//Configure mongoose's promise to global promise
mongoose.promise = global.Promise;

//Configure isProduction variable
const isProduction = process.env.NODE_ENV === 'production';

//Initiate our app
const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//Configure our app
app.use(cors());

app.use('/login', login);

/*app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});*/

function login(req, res, next) {
    if(req.body.user.email === "holger.holgerson@bytabo.de") {
        res.send({_id: "a", name: "Holger", mail: req.body.user.email});
    } else {
        res.status(400).json({message: "Credentials invalid"});
    }
}

app.listen(8000, () => console.log('Server running on http://localhost:8000/'));
